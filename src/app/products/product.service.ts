import { compileNgModuleFromRender2 } from "@angular/compiler/src/render3/r3_module_compiler";
import { Injectable } from "@angular/core";
import { lugares, hotel } from "./products.model";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  private hotele: hotel[] = [
    {
      cuartos: 2,
      banos: 1,
      cocina: "Si",
      sala: "Si",
      piscina: "No",
      codigo: 1,
    },
    {
      cuartos: 1,
      banos: 1,
      cocina: "No",
      sala: "Si",
      piscina: "No",
      codigo: 2,
    },
    {
      cuartos: 3,
      banos: 1,
      cocina: "Si",
      sala: "Si",
      piscina: "Si",
      codigo: 3,
    },
    {
      cuartos: 4,
      banos: 2,
      cocina: "Si",
      sala: "Si",
      piscina: "Si",
      codigo: 4,
    },
    {
      cuartos: 4,
      banos: 2,
      cocina: "Si",
      sala: "Si",
      piscina: "No",
      codigo: 5,
    },
  ];
  private lugares: lugares[] = [
    {
      hotel: this.hotele,
      sitio: "Miravalles",
      nombre: "La cazona Condesa",
      ubicacion: "Guanacaste",
      precio: "200$",
      fecha_disponibilidad: "20-12-2020",
      descripcion: "Espaciosa",
      codigo: 1,
    },
    {
      hotel: this.hotele,
      sitio: "Irazu",
      nombre: "Itachi Hotel",
      ubicacion: "Cartago",
      precio: "300$",
      fecha_disponibilidad: "20-12-2020",
      descripcion: "Cerca del Volcan",
      codigo: 2,
    },
    {
      hotel: this.hotele,
      sitio: "Miraflores",
      nombre: "La reina Lisa",
      ubicacion: "Puntarenas",
      precio: "200$",
      fecha_disponibilidad: "21-12-2020",
      descripcion: "Cerca de la playa",
      codigo: 3,
    },
    {
      hotel: this.hotele,
      sitio: "Flamingo",
      nombre: "Ills Resort",
      ubicacion: "Guanacaste",
      precio: "400$",
      fecha_disponibilidad: "21-12-2020",
      descripcion: "Lujosa",
      codigo: 4,
    },
    {
      hotel: this.hotele,
      sitio: "Manzanillo",
      nombre: "La caribeña",
      ubicacion: "Limon",
      precio: "250$",
      fecha_disponibilidad: "22-12-2020",
      descripcion: "Ambiente calido",
      codigo: 5,
    },
  ];
  constructor() {}
  getAll() {
    return [...this.lugares];
  }

  getLugar(lugarId: number) {
    return {
      ...this.lugares.find((lugar) => {
        return lugar.codigo === lugarId;
      }),
    };
  }

  deleteLugar(lugarId: number) {
    this.lugares = this.lugares.filter((lugar) => {
      return lugar.codigo !== lugarId;
    });
  }

  addLugar(
    psitio: string,
    pnombre: string,
    pubicacion: string,
    pprecio: string,
    pfecha_disponibilidad: string,
    pcodigo: number,
    pdescripcion: string
  ) {
    const lugar: lugares = {
      hotel: this.hotele,
      sitio: psitio,
      nombre: pnombre,
      ubicacion: pubicacion,
      precio: pprecio,
      fecha_disponibilidad: pfecha_disponibilidad,
      codigo: pcodigo,
      descripcion: pdescripcion,
    };
    this.lugares.push(lugar);
  }

  editLugar(
    psitio: string,
    pnombre: string,
    pubicacion: string,
    pprecio: string,
    pfecha_disponibilidad: string,
    pcodigo: number,
    pdescripcion: string
  ) {
    let index = this.lugares.map((x) => x.codigo).indexOf(pcodigo);

    this.lugares[index].sitio = psitio;
    this.lugares[index].nombre = pnombre;
    this.lugares[index].ubicacion = pubicacion;
    this.lugares[index].precio = pprecio;
    this.lugares[index].fecha_disponibilidad = pfecha_disponibilidad;
    this.lugares[index].codigo = pcodigo;
    this.lugares[index].descripcion = pdescripcion;

    /* let myObj = this.lugares.find(ob => ob.codigo = pcodigo);

    myObj.candidad= pcantidad;
    myObj.precio= pprecio;
    myObj.nombre= pnombre;
    myObj.peso= ppeso;
    myObj.descripcion= pdescripcion;
    myObj.fecha_caducidad= pfecha_caducidad;

    this.lugares[index] = myObj;*/

    console.log(this.lugares);
  }
}
